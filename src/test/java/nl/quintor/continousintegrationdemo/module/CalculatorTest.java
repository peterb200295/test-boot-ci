package nl.quintor.continousintegrationdemo.module;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;


@RunWith(SpringRunner.class)
public class CalculatorTest {
    private Calculator calculator = new Calculator();

    @Test
    public void add() {
        Integer expectation = 2;
        Integer result = calculator.add(1);

        assertEquals(expectation, result);
    }

    @Test
    public void add_WhenNumberIsZero() {
        Integer expectation = 1;
        Integer result = calculator.add(0);

        assertEquals(expectation, result);
    }

    @Test
    public void add_WhenNumberIsSmallerThenZero() {
        Integer expectation = 0;
        Integer result = calculator.add(-5);

        assertEquals(expectation, result);
    }
}