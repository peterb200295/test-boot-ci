package nl.quintor.continousintegrationdemo.module;

import org.springframework.stereotype.Component;

@Component
public class Calculator {
    public Integer add(Integer number){
        if (number > -1)
            return number + 1;
        return 0;
    }
}
