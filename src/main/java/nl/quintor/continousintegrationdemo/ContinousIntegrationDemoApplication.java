package nl.quintor.continousintegrationdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ContinousIntegrationDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(ContinousIntegrationDemoApplication.class, args);
	}

}
